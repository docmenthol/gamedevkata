import { Response } from 'https://deno.land/x/oak@v9.0.1/mod.ts'

type RequestParams<T> =
  { params: T
  , response: Response
  }

export type { RequestParams }