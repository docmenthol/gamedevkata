import { Application, Context } from 'https://deno.land/x/oak@v9.0.1/mod.ts'
import { oakCors } from 'https://deno.land/x/cors@v1.2.2/mod.ts'
import router from './routes.ts'
import config from './config.ts'

const app = new Application()

// A man is not dead while his name is still spoken.
app.use(async (ctx: Context, next) => {
  ctx.response.headers.set('X-Clacks-Overhead', 'GNU Terry Pratchett')
  await next();
})

app.use(oakCors({
  origin: /^.+localhost:3000$/,
  optionsSuccessStatus: 200
}))

app.use(router.routes())
app.use(router.allowedMethods())

app.addEventListener('listen', () => {
  console.log(`Listening on port ${config.port}.`)
})

await app.listen({ port: config.port })