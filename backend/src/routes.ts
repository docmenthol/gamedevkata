import { Router } from 'https://deno.land/x/oak@v9.0.1/mod.ts'
import { authConfig, hello } from './controllers.ts'

const router = new Router()
router.get('/auth_config.json', authConfig)
      .get('/hello/:name', hello)

export default router