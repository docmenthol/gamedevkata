import { RequestParams } from '../types.d.ts'

const authConfig = ({ params, response }: RequestParams<never>) => {
  response.body = {
    domain: 'YOUR_DOMAIN',
    clientId: 'YOUR_CLIENT_ID'
  }
  response.status = 200
}

export default authConfig