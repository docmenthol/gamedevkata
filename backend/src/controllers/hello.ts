import { RequestParams } from '../types.d.ts'

type HelloParams =
  { name : string }

const hello = ({ params, response }: RequestParams<HelloParams>) => {
  response.body = `Hello, ${params.name}.`
  response.status = 200
}

export default hello