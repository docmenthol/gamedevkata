# Gamedev Kata

A personal daily progress tracker for gamedev kata: a set of small, repeatable
tasks meant to help you internalize the building blocks of video game
development.

## Premise

tl;dr a managed, prepopulated todo list that can have new todos added but
never removed

Gamedev Kata are organized into two groups: concept and engine. A user can
select some number of any of these kata to start and then check them off every
day. As the user gains confidence in their kata, new ones can be added. The
catch, however, is that once a kata is added to a user's list, it can never be
removed. That's the point: always push forward.

### What are kata?

In martial arts, kata are choreographed series of movements and strikes
indended to teach foundational principles of the martial art: movement, balance,
transition, centering, etc. They are not meant to teach someone to fight, but
instead are tools used for study. They are meant to be examined, understood,
and learned from.

### Concept Kata

These kata will encapsulate skills that are wholly detached from game engines
entirely. A good example of these would be mathematics, such as working with
vectors, and are not intended to be done in code or even on a computer. These
will be best done on paper, a whiteboard, an iPad, or whatever.

### Engine Kata

Contrast to concept kata, these are fully intended to be done in your game engine
or programming language or choice. These will include things like basic rendering
concepts, programming constructs and techniques, and other things like that.
These will be written to be engine-agnostic, but should be practiced within the
environment you wish to use to develop your first real game.