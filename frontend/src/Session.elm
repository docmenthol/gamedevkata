module Session exposing (..)

import Browser.Navigation as Nav
import Viewer exposing (Viewer)



{- Normally, all of this would handle guest *and* authenticated
   users, but for now I'm going to treat everyone as a guest
   until I get all the Auth0 stuff sorted.
-}


type Session
    = Guest Nav.Key


viewer : Session -> Maybe Viewer
viewer _ =
    Nothing


navKey : Session -> Nav.Key
navKey (Guest key) =
    key


fromViewer : Nav.Key -> Maybe Viewer -> Session
fromViewer key _ =
    Guest key
