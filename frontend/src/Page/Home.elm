module Page.Home exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Html exposing (Html, a, div, text)
import Route
import Session exposing (Session)


type alias Model =
    Session


type Msg
    = NoOp


init : Session -> ( Model, Cmd Msg )
init session =
    ( session, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update _ model =
    ( model, Cmd.none )


view : Model -> { title : String, content : Html Msg }
view _ =
    { title = "Home"
    , content =
        div
            []
            [ text "Home page." ]
    }


toSession : Model -> Session
toSession =
    identity
