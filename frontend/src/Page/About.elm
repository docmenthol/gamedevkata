module Page.About exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Html exposing (Html, div, text)
import Session exposing (Session)


type alias Model =
    Session


type Msg
    = NoOp


init : Session -> ( Model, Cmd Msg )
init session =
    ( session, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update _ model =
    ( model, Cmd.none )


view : Model -> { title : String, content : Html Msg }
view _ =
    { title = "About"
    , content =
        div
            []
            [ text "About page." ]
    }


toSession : Model -> Session
toSession =
    identity
