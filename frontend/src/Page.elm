module Page exposing (Page(..), view)

import Browser exposing (Document)
import Html exposing (Html, a, div, h1, main_, nav, node, section, text)
import Html.Attributes exposing (class, href, rel)
import Route
import Viewer exposing (Viewer)


type Page
    = NotFound
    | Other
    | Home
    | About


view : Maybe Viewer -> Page -> { title : String, content : Html msg } -> Document msg
view _ _ { title, content } =
    { title = title ++ " :: Gamedev Kata"
    , body =
        [ nav
            [ class "navbar is-primary" ]
            [ div
                [ class "navbar-brand" ]
                [ a [ class "navbar-item", Route.href Route.Home ] [ text "Gamedev Kata" ] ]
            , div
                [ class "navbar-menu" ]
                [ div
                    [ class "navbar-start" ]
                    [ a [ class "navbar-item", Route.href Route.Home ] [ text "Home" ]
                    , a [ class "navbar-item", Route.href Route.About ] [ text "About" ]
                    ]
                ]
            , div
                [ class "navbar-end" ]
                [ div
                    [ class "navbar-item" ]
                    [ div
                        [ class "buttons" ]
                        [ a [ class "button is-primary" ] [ text "Login" ]
                        , a [ class "button is-light" ] [ text "Sign Up" ]
                        ]
                    ]
                ]
            ]
        , div
            []
            [ section [ class "hero is-primary is-medium is-bold" ]
                [ div
                    [ class "hero-body" ]
                    [ div
                        [ class "container" ]
                        [ h1 [ class "title is-2" ] [ text "Gamedev Kata" ]
                        , div [ class "subtitle is-3" ] [ text "A different approach for learning video game development." ]
                        ]
                    ]
                ]
            , main_ [] [ content ]
            ]
        ]
    }
