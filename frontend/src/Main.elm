module Main exposing (main)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Html
import Page
import Page.About as About
import Page.Blank as Blank
import Page.Home as Home
import Page.NotFound as NotFound
import Route exposing (Route)
import Session exposing (Session)
import Url


type Model
    = NotFound Session
    | Redirect Session
    | Home Home.Model
    | About About.Model


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | NotFoundMsg NotFound.Msg
    | HomeMsg Home.Msg
    | AboutMsg About.Msg


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ _ key =
    let
        session =
            Session.Guest key

        ( homeModel, _ ) =
            Home.init session
    in
    ( Home homeModel
    , Cmd.none
    )


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session =
            toSession model
    in
    case maybeRoute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just Route.Root ->
            ( model, Route.replaceUrl (Session.navKey session) Route.Home )

        Just Route.Home ->
            Home.init session
                |> updateWith Home HomeMsg model

        Just Route.About ->
            About.init session
                |> updateWith About AboutMsg model


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Redirect _ ->
            Sub.none

        NotFound notFound ->
            Sub.map NotFoundMsg <| NotFound.subscriptions notFound

        Home home ->
            Sub.map HomeMsg <| Home.subscriptions home

        About about ->
            Sub.map AboutMsg <| About.subscriptions about


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( LinkClicked request, _ ) ->
            case request of
                Browser.Internal url ->
                    ( model
                    , Nav.pushUrl
                        (Session.navKey <| toSession model)
                        (Url.toString url)
                    )

                Browser.External href ->
                    ( model, Nav.load href )

        ( UrlChanged url, _ ) ->
            changeRouteTo (Route.fromUrl url) model

        ( NotFoundMsg subMsg, NotFound notFound ) ->
            NotFound.update subMsg notFound
                |> updateWith NotFound NotFoundMsg model

        ( NotFoundMsg _, _ ) ->
            ( model, Cmd.none )

        ( HomeMsg subMsg, Home home ) ->
            Home.update subMsg home
                |> updateWith Home HomeMsg model

        ( HomeMsg _, _ ) ->
            ( model, Cmd.none )

        ( AboutMsg subMsg, About about ) ->
            About.update subMsg about
                |> updateWith About AboutMsg model

        ( AboutMsg _, _ ) ->
            ( model, Cmd.none )


updateWith : (subModel -> Model) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg _ ( subModel, subCmd ) =
    ( toModel subModel, Cmd.map toMsg subCmd )


view : Model -> Document Msg
view model =
    let
        viewer =
            Session.viewer <| toSession model

        viewPage page toMsg config =
            let
                { title, body } =
                    Page.view viewer page config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        Redirect session ->
            Page.view viewer Page.Other Blank.view

        NotFound session ->
            viewPage Page.NotFound NotFoundMsg <| NotFound.view session

        Home home ->
            viewPage Page.Home HomeMsg <| Home.view home

        About about ->
            viewPage Page.About AboutMsg <| About.view about


toSession : Model -> Session
toSession model =
    case model of
        Redirect session ->
            session

        NotFound session ->
            session

        Home home ->
            Home.toSession home

        About about ->
            About.toSession about


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }
