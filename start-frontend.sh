#!/bin/bash
cd frontend
# elm reactor --port=3000
elm-live src/Main.elm --port=3000 --open --start-page=index.html --dir=public -- --debug --output=public/app.min.js
cd ..